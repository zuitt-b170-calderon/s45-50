export default [
	{
		id: "wdc001",
		name: "PHP - Laravel",
		description: "Lorem ipsum dolor sit amet, consectetur adipising elit. Vivamus ut dui erat. Aenean a imperdiet",
		price: 45000,
		onOffer: true
	}
	{
		id: "wdc002",
		name: "Python - Django",
		description: "Lorem ipsum dolor sit amet, consectetur adipising elit. Vivamus ut dui erat. Aenean a imperdiet",
		price: 45000,
		onOffer: true
	}
	{
		id: "wdc003",
		name: "Java - Springboot",
		description: "Lorem ipsum dolor sit amet, consectetur adipising elit. Vivamus ut dui erat. Aenean a imperdiet",
		price: 45000,
		onOffer: true
	}
]