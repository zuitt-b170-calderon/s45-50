import React, { useState, useEffect, useContext } from 'react';
import { Navigate } from 'react-router-dom';

// App Imports
import UserContext from '../userContext'

// Bootstrap Components
import { Form, Container, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function Login() {

    const { user, setUser } = useContext(UserContext);

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [ isDisabled, setIsDisabled ] = useState(true);

    useEffect(()=>{
        let isEmailNotEmpty = email !== '';
        let isPasswordNotEmpty = password !== '';

        if ( isEmailNotEmpty && isPasswordNotEmpty ) {
            setIsDisabled(false);
        } else {
            setIsDisabled(true);
        }
        },[ email, password ]);

    function login(e) {
        e.preventDefault();
        Swal.fire('You are now logged in.');
        // localStorage.setItem - to store a piece of data inside the localStorage of the browser. this is because the local storage does not delete unless it manually done so or the codes make it delete the information inside
        localStorage.setItem('email', email);
        setUser( { email: email } );

        setEmail('');
        setPassword('');
    }    

    if (user.email !== null) {
        // return <Redirect to="/" /> - Redirect is a depricated function inside React and it is replaced by Navigate
        /*
            Navigate - allows us to redirect the users after logging in and updatiung the global user state. even if the user tries to input the /login as URI, it would still redirect him/her to the home page
                replace to attribute - to specify the page/uri to where the user/s will be redirected
        */
        return <Navigate replace to="/courses" />
    }

    return (
        <Container fluid>
            <h3>Login</h3>
            <Form onSubmit={login}>
                <Form.Group>
                    <Form.Label>Email address</Form.Label>
                    <Form.Control type="email" placeholder="Enter email" value={email} onChange={(e) => setEmail(e.target.value)} required/>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" placeholder="Password" value={password} onChange={(e) => setPassword(e.target.value)} required/>
                </Form.Group>
                <Button variant="success" type="submit" disabled={isDisabled}>Login</Button>
            </Form>
        </Container>
    )
}