// Base Imports
import React, { useEffect, useState } from 'react';
import { Navigate } from 'react-router-dom';

// Bootstrap Components
/*import Form from 'react-bootstrap/Form';
import Container from 'react-bootstrap/Container';
import Button from 'react-bootstrap/Button';*/

import {Form, Container, Button} from 'react-bootstrap';
import Swal from 'sweetalert2'; //npm install sweetalert2

export default function Register(){

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [passwordConfirm, setPasswordConfirm] = useState('');
	const [isDisabled, setIsDisabled] = useState(true);

	useEffect(()=>{
		let isEmailNotEmpty = email !=='' ;
		let isPasswordNotEmpty = password !=='' ;
		let isPasswordConfirmNotEmpty = passwordConfirm !=='' ;
		let isPasswordMatch = password === passwordConfirm;

		if (isEmailNotEmpty && isPasswordNotEmpty && isPasswordConfirmNotEmpty && isPasswordMatch){
			setIsDisabled(false);
		}else {
			setIsDisabled(true);
		}

	},[email, password, passwordConfirm]);

	/*
	TWO_WAY BINDING

		to be able to capture/save the input value from the input elements, we can bind the value of the element with the states. We, as devs, cannot type into the inputs anymore because there is now value that is bound to it

		Two-way binding is done so that we can assure that we can save the input into our states as the users type into the element. This is so that we dont have to save it before submitting

		
	*/

	function register(e){
		e.preventDefault();
		Swal.fire('Register successful, you may now log in.')

		// clears the input fields since they update their respective variable values into an empty string

		setEmail('');
		setPassword('');
		setPasswordConfirm('');
		return <Navigate replace to= "/courses" />
	}


	
	
	



	/*useEffect(()=>{
		console.log(email);
	},[email]);

	useEffect(()=>{
		console.log(password);
	},[password]);

	useEffect(()=>{
		console.log(passwordConfirm);
	},[passwordConfirm]);*/

	/*
	e.target.value
	e - the event to which the element will listen
	target - the element that is target of the event
	value - the value that the user has entered in that element
	*/

	return(
		<Container>
			<Form onSubmit={register}>
				<Form.Group>
					<Form.Label>Email address</Form.Label>
					<Form.Control type="email" placeholder="Enter email" value={email} onChange={(e)=> setEmail(e.target.value)} required/>
					<Form.Text className='text-muted'>We'll never share your email to anyone else</Form.Text>
				</Form.Group>
				<Form.Group>
					<Form.Label>Password</Form.Label>
					<Form.Control type="password" placeholder="Enter password" value={password} onChange={(e)=> setPassword(e.target.value)} required/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Confirm password</Form.Label>
					<Form.Control type="password" placeholder="Confirm password" value={passwordConfirm} onChange={(e)=> setPasswordConfirm(e.target.value)} required/>
				</Form.Group>
				<Button variant="primary" type="submit" disabled={isDisabled}>Submit</Button>
			</Form>
		</Container>
		)
} 