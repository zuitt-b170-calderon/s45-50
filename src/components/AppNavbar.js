import React, {Fragment, useContext} from 'react';
import { Link, NavLink, useNavigate } from 'react-router-dom';

// Bootstrap Components
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';

/*
  export default - allows the function to be used by other files outside the file
*/

//App imports
import UserContext from '../userContext.js';

export default function AppNavbar(){

  const {user, unsetUser} = useContext(UserContext);
  const navigate = useNavigate();

  const logout = () => {
    unsetUser();
    navigate('/login');
  }


  let rightNav = (user.email === null)?(
    <Fragment>
      <Nav.Link as={NavLink} to='/register'>Register</Nav.Link>
      <Nav.Link as={NavLink} to='/login'>Login</Nav.Link>
    </Fragment>
    ) : (
    <Fragment>
      <Nav.Link onClick = {logout}>Logout</Nav.Link>
    </Fragment>
    )

  return(
      <Navbar bg='light' expand='lg'>
        <Navbar.Brand as={Link} to='/'>Zuitt Booking</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="mr-auto">
                <Nav.Link as={NavLink} to='/'>Home</Nav.Link>
                <Nav.Link as={NavLink} to='/courses'>Courses</Nav.Link>
            </Nav>
            <Nav className = 'ml-auto'>
              {rightNav}
            </Nav>
        </Navbar.Collapse>
      </Navbar>
    )
}